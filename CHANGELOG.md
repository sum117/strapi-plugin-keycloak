# Changelog

## 0.0.25

Use `post_logout_redirect_uri` instead of `redirect_uri` for `logout` redirects according to the [OpenID specification](https://openid.net/specs/openid-connect-rpinitiated-1_0.html#RPLogout). This is required especially after the upgrade to Keycloak 18.

## 0.0.24

Fixed a bug where the logout would not actually log the user out.

## 0.0.23

Add the `jwtPublicKey` and `jwtAlgorithm` configuration properties to get the login status and the user profile from the access token instead of requiring a fetch from the Keycloak `userinfo` endpoint each time.

## 0.0.22

When processing Strapi API tokens, treat the "Bearer" portion of the code without case sensitivity (e.g. `bearer` and `Bearer` are both accepted).

## 0.0.21

The middleware now checks for Strapi API tokens and, if one is set, doesn't require a Keycloak login. This is in line with the way that Strapi API tokens are designed: To bypass other authorization strategies.

## 0.0.20

Populate `ctx.state.keycloak.profile` with the current user's Keycloak profile. Using that, you can access user information in Strapi code.

## 0.0.19

Introduced `redirectTo` query parameter to the `logout` endpoint.
