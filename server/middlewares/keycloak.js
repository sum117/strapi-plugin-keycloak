"use strict";

const getLoginStatusAndProfile = require("../utils/get-login-status-and-profile");
const verifyApiToken = require("../utils/verify-api-token");

/**
 * `keycloak` middleware.
 */

module.exports = () => {
  return async (ctx, next) => {
    if (await verifyApiToken(ctx)) {
      await next();
      return;
    }

    const { isLoggedIn, profile } = await getLoginStatusAndProfile(ctx);
    if (!isLoggedIn) {
      ctx.status = 403;
      ctx.body = "Please supply header Authorization or have a valid session.";
      return;
    }

    if (!ctx.state) {
      ctx.state = {};
    }

    ctx.state.keycloak = {
      profile,
    };

    await next();
  };
};
