const getAccessTokenFromContext = require("./get-access-token-from-context");

describe("getAccessTokenFromContext", () => {
  it("should return undefined if no access token is set in the context", () => {
    expect(getAccessTokenFromContext({})).toBeUndefined();
  });

  it("should return the access token from the session if one is set", () => {
    expect(
      getAccessTokenFromContext({
        session: { keycloak: { accessToken: "abc" } },
      })
    ).toEqual("abc");
  });

  it("should return the access token from the headers if one is set", () => {
    expect(
      getAccessTokenFromContext({
        headers: { keycloak: "abc" },
      })
    ).toEqual("abc");
  });

  it("should return the access token from the headers if one is set and remove a 'Bearer' prefix", () => {
    expect(
      getAccessTokenFromContext({
        headers: { keycloak: "Bearer abc" },
      })
    ).toEqual("abc");
  });
});
