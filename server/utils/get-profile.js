const fetch = require("node-fetch");
const getAccessTokenFromContext = require("./get-access-token-from-context");
const { getJwtValidityAndPayload } = require("./jwt");

module.exports = async (ctx) => {
  const { afterRetrieveProfile, onRetrieveProfile } = strapi.config.keycloak;

  const {
    userinfoEndpoint,
    alwaysFetchProfileFromKeycloak,
    jwtPublicKey,
    jwtAlgorithm = "RS256",
  } = strapi.config.keycloak;

  const accessToken = getAccessTokenFromContext(ctx);

  if (!accessToken) {
    return null;
  }

  if (jwtPublicKey && !alwaysFetchProfileFromKeycloak) {
    const { payload } = getJwtValidityAndPayload(
      accessToken,
      jwtAlgorithm,
      jwtPublicKey
    );
    return payload;
  }

  // if no JWT public key is set, fall back to contacting Keycloak for details

  // verify that user is valid by getting user info
  const userInfoResponse = await fetch(userinfoEndpoint, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });

  let userInfo = await userInfoResponse.json();

  if (onRetrieveProfile) {
    const additionalProfileInfos = await onRetrieveProfile(ctx);
    userInfo = {
      ...userInfo,
      ...additionalProfileInfos,
    };
  }

  if (afterRetrieveProfile) {
    await afterRetrieveProfile(ctx, userInfo);
  }

  return userInfo;
};
