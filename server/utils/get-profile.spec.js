const jwt = require("jsonwebtoken");
const getProfile = require("./get-profile");
jest.mock("node-fetch", () => jest.fn());
const fetch = require("node-fetch");

describe("getProfile", () => {
  beforeEach(() => {
    global.strapi = {
      config: {
        keycloak: {},
      },
    };
  });

  it("sould return null if no access token is set", async () => {
    const result = await getProfile({});
    expect(result).toBeNull();
  });

  it("sould use the access token's payload if a JWT public key is set", async () => {
    // setup
    global.strapi = {
      config: {
        keycloak: {
          jwtPublicKey: "abcsiofhodighoidhgosdhgoih",
          jwtAlgorithm: "HS256",
        },
      },
    };

    const userProfile = {
      a: "b",
    };

    const ctx = {
      headers: {
        keycloak: jwt.sign(userProfile, "abcsiofhodighoidhgosdhgoih", {
          algorithm: "HS256",
        }),
      },
    };

    // test
    const result = await getProfile(ctx);

    // assert
    expect(result).toEqual({
      ...userProfile,
      iat: expect.anything(),
    });
  });

  it("should fetch the user profile from Keycloak if no JWT public key is set", async () => {
    // setup
    const userProfile = {
      a: "b",
    };

    const ctx = {
      headers: {
        keycloak: jwt.sign(userProfile, "abcsiofhodighoidhgosdhgoih", {
          algorithm: "HS256",
        }),
      },
    };

    fetch.mockResolvedValue({
      json: jest.fn().mockResolvedValue(userProfile),
    });

    // test
    const result = await getProfile(ctx);

    // assert
    expect(result).toEqual(userProfile);
  });
});
