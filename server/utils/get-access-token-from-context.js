module.exports = (ctx) => {
  let accessToken = ctx.session?.keycloak?.accessToken || ctx.headers?.keycloak;

  if (!accessToken) {
    return;
  }

  if (accessToken.startsWith("Bearer ")) {
    accessToken = accessToken.substr("Bearer ".length);
  }

  return accessToken;
};
