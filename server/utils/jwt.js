const jwt = require("jsonwebtoken");

function getJwtCertificateFromPublicKey(publicKey) {
  return `-----BEGIN PUBLIC KEY-----
${publicKey}
-----END PUBLIC KEY-----`;
}

function getSecretOrPublicKeyBasedOnAlgorithm(publicKey, algorithm) {
  return algorithm !== "HS256"
    ? getJwtCertificateFromPublicKey(publicKey)
    : publicKey;
}

function getJwtValidityAndPayload(token, algorithm, publicKey) {
  try {
    const payload = jwt.verify(
      token,
      getSecretOrPublicKeyBasedOnAlgorithm(publicKey, algorithm),
      {
        algorithms: [algorithm],
      }
    );
    return {
      valid: true,
      payload,
    };
  } catch (err) {
    return { valid: false, payload: null };
  }
}

module.exports = {
  getJwtValidityAndPayload,
};
