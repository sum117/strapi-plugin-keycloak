module.exports = {
  env: {
    browser: false,
    commonjs: true,
    es2021: true,
    "jest/globals": true,
  },
  plugins: ["eslint-plugin-jest"],
  extends: ["eslint:recommended", "plugin:prettier/recommended", "prettier"],
  parserOptions: {
    ecmaVersion: "latest",
  },
  rules: {},
  globals: {
    strapi: "readonly",
    URL: "readonly",
  },
};
